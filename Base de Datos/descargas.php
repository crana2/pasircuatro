<?php



// Inicia la sesión en todas lasicio sesion

session_start();



// usuario ha iniciado sesión correctamente en index.php

if (!isset($_SESSION['logged']) || $_SESSION['logged'] !== true) {

    //  redirige a la página de inicio de sesión

    header("Location: index.php");

    exit;

}



?>













<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Descargas de Modelos 3D</title>

    <script type="module" src="https://unpkg.com/@google/model-viewer/dist/model-viewer.min.js"></script>

    <style>

        body {

            margin: 0;

            padding: 0;

            font-family: Arial, sans-serif;

        }

        h1 {

            text-align: center;

            margin-top: 20px;

            margin-bottom: 20px;

        }

        .container {

            display: flex;

            flex-wrap: wrap;

            justify-content: space-around;

            align-items: center;

            background-color: #cde8f6;

            padding: 20px;

        }

        .model-container {

            width: calc(33.33% - 20px);

            height: 0;

            padding-bottom: 30%;

            position: relative;

            border: 10px solid #f0a0c0;

            box-sizing: border-box;

            margin: 10px;

        }

        model-viewer {

            position: absolute;

            width: 100%;

            height: 100%;

        }

        .download-link {

            text-align: center;

            margin-top: 5px;

            display: block;

            position: absolute;

            bottom: 5px;

            width: 100%;

        }

        .small-model {

            width: 70%; /* Reducir el tamañl modelo */

            height: 70%; /* Reducir el tamaño modelo */

        }

    </style>

</head>







<!-- BODY DEL HTML                -->



<body>

    <h1>Descargas de Modelos 3D</h1>

    <div class="container">

        <?php

            include 'configweb.php';



            // Conexión a la base de datos

            $conexion = mysqli_connect($servidor, $usuario, $clave, $bd, $puerto);







            if (!$conexion) {



			$error = mysqli_connect_error();



			$n_error = mysqli_connect_errno();



			header("Location:errorweb.php?error=$error&n_error=$n_error");

 

            }













            // hago la consulta

            $sql = "SELECT ruta_modelo_3d, ruta_zip_descarga FROM Modelo";







		// ejecuto la consulta

            $result = mysqli_query($conexion,$sql);











		// compruebo que devuelve como minimo una fila

            if ( mysqli_num_rows($result) > 0) {



                while ($row = mysqli_fetch_row($result)) {

                    echo "<div class='model-container'>";

                    echo "<model-viewer class='small-model' src='" . $row['ruta_modelo_3d'] . "' camera-controls auto-rotate disable-zoom></model-viewer>";

                    echo "<a class='download-link' href='" . $row['ruta_zip_descarga'] . "' download>Descargar Modelo</a>";

                    echo "</div>";

                }

            } else {

                echo "No se encontraron modelos en la base de datos.";

            }



            mysqli_close($conexion);

        ?>

    </div>

</body>

</html>

